#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Unsupervised clustering using GMM

This codes reads and normalizes the data.
Groups the data into the desired number of distributions using gaussian mixture models.
Then it prints the cluster labels, plots distribution of the labels

lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot')
fig, ax = plt.subplots(1,3, figsize=(24, 8), num='Unsupervised') # subplots and figure sizes
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 20
plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title




# analysis
from sklearn.mixture  import GaussianMixture
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter, OrderedDict
import numpy as np

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-k', metavar='number of Clusters. Should be < 22', type=int)

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.k is not None and arguments.k > 21:
    	print('Error! k should remain 21 or less')
    	sys.exit()
    if arguments.k == None:
    	print('k is not specified, default 3 would be used')
    	arguments.k = 3
    print ('-- Input file:', arguments.i)
except Exception as e:
    parser.error(str(e))
    sys.exit()

# =============================================================================
# %% load, normalize, and find principal components of the data
# =============================================================================
data = pd.read_csv(arguments.i) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values [0-1]

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)

# =============================================================================
# %% optimal number components using BIC
# This criterion gives us an estimation on how much is good the GMM in terms of predicting the data we actually have. The lower is the BIC, the better is the model to actually predict the data we have, and by extension, the true, unknown, distribution. In order to avoid overfitting, this technique penalizes models with big number of clusters.
# =============================================================================
n_components = np.arange(1, 8)
models = [GaussianMixture(n, covariance_type='full', random_state=0).fit(x_norm) for n in n_components]

# plt.figure('BIC', figsize=(7,7))
bic_score = [m.bic(x_norm) for m in models]
ax[0].plot(n_components, bic_score , label='BIC')
aic_score = [m.aic(x_norm) for m in models]
ax[0].plot(n_components, aic_score , label='AIC')
ax[0].set_title('A', loc='left') ### uncomment if you want to have the lable within the figure
ax[0].set_xticks(n_components)
ax[0].set_xlabel('No. of clusters', fontsize=BIGGER_SIZE)
ax[0].set_ylabel('Score', fontsize=BIGGER_SIZE)
ax[0].legend()

ax[0].annotate('Chosen value', xy=(3, aic_score[2]), xytext=(4, 1.2*aic_score[2]), arrowprops=dict(facecolor='green', shrink=0.01))
ax[0].annotate(' ', xy=(3, bic_score[2]), xytext=(4, 1.2*aic_score[2]), arrowprops=dict(facecolor='green', shrink=0.01))

# =============================================================================
# %% gausian mixture model (GMM) to group the data into the desired number of distributions
# =============================================================================
fs = (7, 7)
color=['red', 'blue', 'green', 'sienna', 'firebrick', 'darkorchid', 'navy', 'gold', 'darkgreen', 'darksalmon', 'mediumvioletred', 'deeppink', 'darkblue', 'tomato', 'brown', 'gray', 'green', 'orange', 'black', 'azure', 'indigo']
n_components = arguments.k # number of components (parameter)
model = GaussianMixture(n_components=n_components, covariance_type='full', random_state=0)
gmm = model.fit(x_norm) # initialize
labels = gmm.predict(x_norm)
label_dist = OrderedDict(Counter(labels).most_common())


# prints
print()
print('cluster indices (each number indicates a cluster): {}'.format(labels))
# print('cluster centers: ', KM.cluster_centers_)

# visualize the whole dataset usingthe principal components
# ax[1].figure('GMM clustering', figsize=fs)
for l in label_dist.keys():
    ax[1].scatter(x_principal[labels==l, 0], x_principal[labels==l, 1] , color=color[l], label=l, cmap='viridis')
ax[1].set_title('B', loc='left')
ax[1].set_xlabel('PC1', fontsize=BIGGER_SIZE)
ax[1].set_ylabel('PC2', fontsize=BIGGER_SIZE)
#plt.legend() # uncomment if you want to display the legend

# bar plot of differen labes after clustering
# plt.figure('Distribution', figsize=fs)
ax[2].bar([str(l) for l in label_dist.keys()],
        list(label_dist.values()),
        color=[color[i] for i in label_dist.keys()],
        align='center')
ax[2].set_title('C', loc='left')
ax[2].set_xlabel('Clusters', fontsize=BIGGER_SIZE)
ax[2].set_ylabel('Counts', fontsize=BIGGER_SIZE)
ax[2].set_xticks([])

# enlarge fonts
for tick in ax[0].get_yticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[0].get_xticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[1].get_yticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[1].get_xticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[2].get_yticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[2].get_xticklabels():
    tick.set_fontsize(MEDIUM_SIZE)

plt.tight_layout()
# plt.savefig("./figs/4",dpi=500, bbox_inches='tight') # save to file
plt.show()
