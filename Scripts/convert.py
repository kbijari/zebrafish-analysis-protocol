#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

This script will convert L-Measure's output into a machine readable csv format.
Please be advised that If you want to use a statistic other than the one mentioned in the manuscript this script won't be useful anymore!

"""

# =============================================================================
# %% required imports
# =============================================================================
import pandas as pd # data management
import numpy as np
import csv
import os
import sys
import argparse


## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-o', metavar='output-file - if left empty output will be written into output.out', type=argparse.FileType('wt'))

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.o == None:
    	print('Output file is not specified, the outputs will be written into output.out in the current directory')
    	arguments.o = open('./output.out', 'wt')
    print ('-- Input file:', arguments.i)
    print ('-- Output file:', arguments.o)
except Exception as e:
    parser.error(str(e))
    sys.exit()



# =============================================================================
# %% load and convert the data
# =============================================================================
data = pd.read_csv(arguments.i, sep='\t',header=None) # read the data from the directory it is located
data.reset_index(drop=True, inplace=True)

# priorities
selection = [0, 0, 4, 4, 4, 0, 5, 5, 5, 4, 0, 4, 4, 4, 4]

processed = []
w = 0
tmp = []
header = ['neuron']

for idx, row in data.iterrows():
    if w == 10: # target midlist to keep track of the neuron name
        name = [row[0]]
    if len(header) <= 15:
        header.append(row[1].strip())
    if w == len(selection):
        print('processing ...', row[0])
        w = 0
        processed.append(name + tmp)
        tmp = []
    r = row[2:].to_list()
    tmp.append(r[selection[w]])
    w +=1
else:
    # tmp.append(r[selection[w]])
    processed.append(name+tmp)


### write to file
write = csv.writer(arguments.o)
write.writerow(header) 
write.writerows(processed) 
