#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Unsupervised clustering using Kmeans algorithm

This codes reads and normalizes the data.
Clusters the data into the desired number of clusters using Kmeans algorithm.
Then it prints the cluster labels, plots distribution of the labels.
lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management
import numpy as np

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot')
fig, ax = plt.subplots(1,3, figsize=(24, 8), num='Unsupervised') # subplots and figure sizes
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 20
plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# analysis
from sklearn.mixture  import GaussianMixture
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-k', metavar='number of Clusters. Should be < 22', type=int)

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.k is not None and arguments.k > 21:
    	print('Error! k should remain 21 or less')
    	sys.exit()
    if arguments.k == None:
    	print('k is not specified, default 3 would be used')
    	arguments.k = 3
    print ('-- Input file:', arguments.i)
except Exception as e:
    parser.error(str(e))
    sys.exit()


# =============================================================================
# %% load data
# =============================================================================
data = pd.read_csv(arguments.i, ) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values [0-1]

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)


# =============================================================================
# %% optimal number of clusters using elbow method
# =============================================================================
Error = [] # keep track of the error per Kmeans clustering for different number of clusters
rng = 11
for i in range(1, rng):
    kmeans = KMeans(n_clusters = i).fit(x)
    kmeans.fit(x)
    Error.append(kmeans.inertia_)
    
# ax[0].figure('Elbow curve', figsize=(7,7))
ax[0].plot(range(1, rng), Error)
#ax[0].title('Elbow curve') ### uncomment if you want to have the lable within the figure
ax[0].set_title('    A', loc='left')
ax[0].set_xticks(range(1, rng))
ax[0].set_xlabel('No. of clusters', fontsize=BIGGER_SIZE)
ax[0].set_ylabel('Error', fontsize=BIGGER_SIZE)

# ax[0].annotate('Chosen value', xy=(3, Error[2]), xytext=(5, 2*Error[2]), arrowprops=dict(facecolor='green', shrink=0.01))


# =============================================================================
# %% Kmeans clustering to cluster the data into the desired number of clusters
# =============================================================================
clusters = arguments.k # number of clusters (parameter)
KM = KMeans(n_clusters=clusters) # initialize the clustering method
labels = KM.fit_predict(x_norm)
label_dist = Counter(labels)


# prints
print()
print('cluster indices (each number represents a cluster): {}'.format(labels))
print('cluster centers: ', KM.cluster_centers_)

# visualize the whole dataset usingthe principal components
# fs = (7, 7)
color=['red', 'blue', 'green', 'sienna', 'firebrick', 'darkorchid', 'navy', 'gold', 'darkgreen', 'darksalmon', 'mediumvioletred', 'deeppink', 'darkblue', 'tomato', 'brown', 'gray', 'green', 'orange', 'black', 'azure', 'indigo']
# ax[X].figure('K-means clustering', figsize=fs)
for l in label_dist.keys():
    ax[1].scatter(x_principal[labels==l, 0],x_principal[labels==l, 1] , color=color[l], label=l, cmap='rainbow')
ax[1].set_title('B', loc='left') ### uncomment if you want to have the lable within the figure
ax[1].set_xlabel('PC1', fontsize=BIGGER_SIZE)
ax[1].set_ylabel('PC2', fontsize=BIGGER_SIZE)
#plt.legend() # uncomment if you want to make legend for the figure indicating which color corresponds to which number


### bar plot of differen labes after clustering
# ax[x].figure('Distribution', figsize=fs)
ax[2].bar([str(l) for l in label_dist.keys()],
        list(label_dist.values()),
        color=[color[i] for i in label_dist.keys()],
        align='center')
ax[2].set_title('C', loc='left') ### uncomment if you want to have the lable within the figure
ax[2].set_xlabel('Clusters', fontsize=BIGGER_SIZE)
ax[2].set_ylabel('Counts', fontsize=BIGGER_SIZE)
ax[2].set_xticks([])

# enlarge fonts
for tick in ax[0].get_yticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[0].get_xticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[1].get_yticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[1].get_xticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[2].get_yticklabels():
    tick.set_fontsize(MEDIUM_SIZE)
for tick in ax[2].get_xticklabels():
    tick.set_fontsize(MEDIUM_SIZE)


plt.style.use('ggplot')
# plt.savefig("./figs/3",dpi=500, bbox_inches='tight') # save to file
plt.show()
# bbox_inches='tight'
