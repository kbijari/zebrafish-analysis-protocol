#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Supervised classification

This codes reads and normalizes the data.
Reads the labels for the data.
It makes density plot for feature 'Contraction' based on the whole data and its sub classes.

lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot')

# analysis
from sklearn.cluster import KMeans
from sklearn.mixture  import GaussianMixture
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter
import numpy as np

# density
from scipy.stats import gaussian_kde


## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-l', metavar='input-labels', type=argparse.FileType('rt'))

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! Please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.l == None:
        print('Label file is not specified! Pleae specify your label data using: -l <your-labels-data>')
        sys.exit()
except Exception as e:
    parser.error(str(e))
    sys.exit()

# =============================================================================
# %% load, normalize, and find principal components of the data
# =============================================================================
data = pd.read_csv(arguments.i) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)

# read labes for the data
data_labels = pd.read_csv(arguments.l) # read data labels from where it is located

if len(data) != len(data_labels):
    print('There is a missmatch between data and the labels. Please make sure they have the same size!')
    sys.exit()


print()
print('- which feature you want to do density analysis on?')
print('Select from:', ', '.join(list(data.columns)[1:]))
feat = input('feature name: ')
if feat not in list(data.columns):
    print('The feature is not available in the list of data! Please re-enter or check the spelling.')
    sys.exit()

print()
class_labels = list(data_labels.columns)[1:]
# print(class_labels)
print('- which class label you want to do density analysis on?')
print('Select from:', ', '.join(class_labels))
class_name = input('class label: ')
if class_name not in list(data_labels.columns):
    print('The class name is not available in the list of labels! Please re-enter or check the spelling.')
    sys.exit()

print()
class_values = list(set(data_labels[class_name].to_list()))
rm = True
while rm:
    print('- do you wish to leave out any label from this analysis - enter the name or type "no" to stop?')
    print('Select from:', ', '.join(class_values))
    class_name_rm = input('label to remove: ')
    if class_name_rm.lower() == 'no': break
    if class_name_rm in class_values:
        class_values.remove(class_name_rm)


# =============================================================================
# %% Calculate feature importance &
# =============================================================================
# select the feature you want to apply density analysis on from the list below:
feature = feat #['Contraction' #'N_bifs', 'N_branch', 'Width', 'Height', 'Depth', 'Length', 'EucDistance', 'PathDistance', 'Branch_Order', 'Contraction', 'Fragmentation', 'Partition_asymmetry', 'Bif_ampl_local',       'Bif_ampl_remote', 'Fractal_Dim'
plt.figure('{} density plot'.format(feature))

# Density plot for all the data
y = data[feature].values
x_vals = np.linspace(min(y),max(y), 1000) # Specifying the limits of our data
density = gaussian_kde(y)
density.covariance_factor = lambda : .35  # Smoothing parameter
density._compute_covariance()
plt.plot(x_vals, density(x_vals), '-.', label='Entire data', color='k') # all data density plot

# Select specific class labels to perform density analysis for them.
# Here we select Lateral Line (LL) features. 
# LL contains 2 main subclasses that we are interested in (ALL vs PLL)
# therefore, we make density plot for each of those (ALL and PLL)
# If you select class hemisphere for example, then you should make the curves for 'rigth' and 'left' by changing 'LL' to 'hemisphere' and changing 'ALL' and 'PLL' to 'right' and 'left' respectively.
# markers = ['*', ',', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p', '.', 'h', 'H', '+', 'x', 'D', 'd', '|', '_', 'P', 'X', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
colors  = ['blue', 'red', 'sienna', 'firebrick', 'darkorchid', 'navy', 'olivedarb', 'gold' 'darkgreen', 'darksalmon', 'mediumvioletred', 'deeppink', 'darkblue', 'tomato', 'brown', 'gray', 'green', 'orange','azure', 'indigo']


# Density for ALL
for idx, cl in enumerate(class_values):
    print(idx, cl, class_name)
    y = data[data_labels[class_name]==cl][feature].values
    x_vals = np.linspace(min(y),max(y), 1000) # Specifying the limits of our data
    density = gaussian_kde(y)
    density.covariance_factor = lambda : .35  # Smoothing parameter
    density._compute_covariance()
    plt.plot(x_vals, density(x_vals), '--', label=cl, color=colors[idx]) # all data density plot

plt.legend(loc="upper left")
plt.xlabel(feature)
plt.ylabel('Density')
# plt.title('{} density plot'.format(feature))
plt.tight_layout()
# plt.savefig("./figs/density1",dpi=500, bbox_inches='tight') # save to file
plt.show()
