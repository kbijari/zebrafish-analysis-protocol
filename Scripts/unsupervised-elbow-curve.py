#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Unsupervised elbow curve using Kmeans algorithm

This codes reads and normalizes the data.
Then it prints the elbow curve which can be used to determine the correct number of clusters

lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management


import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot')

# analysis
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0


# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    print ('-- Input file:', arguments.i)
except Exception as e:
    parser.error(str(e))
    sys.exit()


# =============================================================================
# %% load data
# =============================================================================
data = pd.read_csv(arguments.i) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:16].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values [0-1]

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)


# =============================================================================
# %% optimal number of clusters using elbow method
# =============================================================================
Error = [] # keep track of the error per Kmeans clustering for different number of clusters
rng = 16
for i in range(1, rng):
    kmeans = KMeans(n_clusters = i).fit(x)
    kmeans.fit(x)
    Error.append(kmeans.inertia_)
    
plt.figure('Elbow curve', figsize=(7,7))
plt.plot(range(1, rng), Error)
#plt.title('Elbow curve') ### uncomment if you want to have the lable within the figure
plt.xticks(range(1, rng))
plt.xlabel('No. of clusters')
plt.ylabel('Error')

# plt.annotate('Chosen value', xy=(3, Error[2]), xytext=(10, 2*Error[2]),
#             arrowprops=dict(facecolor='green', shrink=0.01))

plt.style.use('ggplot')
# plt.savefig("./figs/2",dpi=500, bbox_inches='tight') # save to file
plt.show()

