#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Unsupervised clustering using GMM

This codes reads and normalizes the data.
Groups the data into the desired number of distributions using gaussian mixture models.
Then it the BIC curve which can be used to determine the correct number of distributions

lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot')



# analysis
from sklearn.mixture  import GaussianMixture
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter, OrderedDict
import numpy as np

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    print ('-- Input file:', arguments.i)
except Exception as e:
    parser.error(str(e))
    sys.exit()

# =============================================================================
# %% load, normalize, and find principal components of the data
# =============================================================================
data = pd.read_csv(arguments.i, ) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values [0-1]

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)


# =============================================================================
# %% optimal number components using BIC
# This criterion gives us an estimation on how much is good the GMM in terms of predicting the data we actually have. The lower is the BIC, the better is the model to actually predict the data we have, and by extension, the true, unknown, distribution. In order to avoid overfitting, this technique penalizes models with big number of clusters.
# =============================================================================
n_components = np.arange(1, 7)
models = [GaussianMixture(n, covariance_type='full', random_state=0).fit(x_norm) for n in n_components]

plt.figure('BIC', figsize=(7,7))
bic_score = [m.bic(x_norm) for m in models]
plt.plot(n_components, bic_score , label='BIC')
aic_score = [m.aic(x_norm) for m in models]
plt.plot(n_components, aic_score , label='AIC')
# plt.set_title('Curves')
plt.xticks(n_components)
plt.xlabel('No of clusters')
plt.ylabel('Score')
plt.legend()

plt.annotate('Chosen value', xy=(3, aic_score[2]), xytext=(5.5, 1.2*aic_score[2]), arrowprops=dict(facecolor='green', shrink=0.01))
plt.annotate(' ', xy=(3, bic_score[2]), xytext=(5.5, 1.2*aic_score[2]), arrowprops=dict(facecolor='green', shrink=0.01))

plt.tight_layout()
# plt.savefig("./figs/4",dpi=500, bbox_inches='tight') # save to file
plt.show()
