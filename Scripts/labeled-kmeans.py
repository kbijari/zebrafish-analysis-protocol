#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Supervised analysis

This codes reads and normalizes the data.
Reads the labels for the data
Clusters the data using Kmeans algorithm into 3 (or desired) number of clusters.
Then it visualizes the data points based on their labels and their clusters.

lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
from matplotlib import colors as cl
plt.style.use('ggplot') # style for the figure



# analysis
from sklearn.metrics.cluster import homogeneity_score
from sklearn.cluster import KMeans
from sklearn.mixture  import GaussianMixture
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter
import numpy as np

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-l', metavar='input-labels', type=argparse.FileType('rt'))
parser.add_argument('-k', metavar='number of Clusters. Should be < 22', type=int)


try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! Please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.l == None:
        print('Label file is not specified! Pleae specify your label data using: -l <your-labels-data>')
        sys.exit()
    if arguments.k == None:
    	print('k is not specified, default 3 would be used')
    	arguments.k = 3
except Exception as e:
    parser.error(str(e))
    sys.exit()

# =============================================================================
# %% load, normalize, and find principal components of the data
# =============================================================================
data = pd.read_csv(arguments.i) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)

# read labes for the data
data_labels = pd.read_csv(arguments.l) # read data labels from where it is located
cols = list(data_labels.columns)[1:]

# =============================================================================
# %% Kmeans clustering to cluster the data into the desired number of clusters
# =============================================================================
clusters = arguments.k # number of clusters (parameter)
KM = KMeans(n_clusters=clusters) # initialize the clustering method
KM_labels = KM.fit_predict(x_norm)

# =============================================================================
# %% visualize based on true labels and clustering labels
# =============================================================================

markers = ['*', ',', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p', '.', 'h', 'H', '+', 'x', 'D', 'd', '|', '_', 'P', 'X', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
colors  = ['blue', 'red', 'sienna', 'firebrick', 'darkorchid', 'navy', 'olivedarb', 'gold' 'darkgreen', 'darksalmon', 'mediumvioletred', 'deeppink', 'darkblue', 'tomato', 'brown', 'gray', 'green', 'orange', 'black', 'azure', 'indigo']

for col in cols:
    var = col
    plt.figure(var.title(), figsize=(7,7))
    data_labels_plt = data_labels[var].to_list()
    label_map = {l: i for i, l in enumerate(set(data_labels_plt))}   # make label map
    true_labels = [label_map[l] for l in data_labels[var].to_list()] # map labels to numbers
    legend_plt = []
    for idx, x in enumerate(x_principal):
        if true_labels[idx] not in legend_plt: # general legend label
            plt.scatter(x[0], x[1], marker=markers[true_labels[idx]], color='black', label=data_labels_plt[idx])
            legend_plt.append(true_labels[idx])
        plt.scatter(x[0], x[1], marker=markers[true_labels[idx]], color=colors[KM_labels[idx]])
    # plt.title(var.title())
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    plt.legend(edgecolor='k')
    print('({}) homogeneity score for ({}) clusters:'.format(col, clusters), homogeneity_score(true_labels, KM_labels))
    # if col == 'region': # save to file
    #     plt.savefig("./figs/reg",dpi=500, bbox_inches='tight') # save to file

# print the costomized figure
plt.tight_layout()
plt.show()
