#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Supervised analysis

This codes reads and normalizes the data.
Reads the labels for the data
Then it visualizes the data points based on their labels.
There are different labels for each neuron. For example, neurons are classified based on 'region', 'hemisphere', and 'neuromast'

lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot') # style for the figure
# fig, ax = plt.subplots(2,3, figsize=(12, 12), num='Supervised') # subplots and figure sizes


# analysis
from sklearn.mixture  import GaussianMixture
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter
import numpy as np

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-l', metavar='input-labels', type=argparse.FileType('rt'))

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! Please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.l == None:
        print('Label file is not specified! Pleae specify your label data using: -l <your-labels-data>')
        sys.exit()    	
except Exception as e:
    parser.error(str(e))
    sys.exit()

# =============================================================================
# %% load, normalize, and find principal components of the data
# =============================================================================
data = pd.read_csv(arguments.i) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)

# read labes for the data

data_labels = pd.read_csv(arguments.l) # read data labels from where it is located
cols = list(data_labels.columns)[1:]

# =============================================================================
# %% visualize the data based on their ground truth labels
# =============================================================================
# iterate and plot based on different classes
for col in cols:
    label_map = {l: i for i, l in enumerate(set(data_labels[col].to_list()))} # make label map
    c = [label_map[l] for l in data_labels[col].to_list()] # map labels to numbers
    plt.figure(col.title(), figsize=(7,7))
    leg = plt.scatter(x_principal[:, 0],x_principal[:, 1] , c=c, cmap='viridis')
    # plt.title(col) # title for the window
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    plt.legend(handles=leg.legend_elements()[0], labels=label_map.keys())
    # if col == 'region': # save to file
    #     plt.savefig("./figs/reg",dpi=500, bbox_inches='tight') # save to file


# print the costomized figure
plt.tight_layout()
plt.show()