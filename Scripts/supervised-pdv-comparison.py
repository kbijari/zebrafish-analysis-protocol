#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Supervised Statistical analysis on PDVs

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys, os
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot') # style for the figure
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18
plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# analysis
from sklearn.cluster import KMeans
from sklearn.mixture  import GaussianMixture
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter
import numpy as np
import statistics

# statistical tests
from scipy.stats import ttest_ind


## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-p', metavar='input-folder', help='path to the folder containing PDV files')
parser.add_argument('-l', metavar='input-labels', type=argparse.FileType('rt'))

try:
    arguments = parser.parse_args()
    if arguments.p == None:
    	print('Path file can not be empty! Please make sure to specify input using: -p <path-to-your-PDV-file>')
    	sys.exit()
    if arguments.l == None:
        print('Label file is not specified! Pleae specify your label data using: -l <your-labels-data>')
        sys.exit()
except Exception as e:
    parser.error(str(e))
    sys.exit()


# =============================================================================
# %% load neuron classes and their PDVs
# =============================================================================

# read labes of the data
data_labels = pd.read_csv(arguments.l) # read data labels from where it is located

print()
class_labels = list(data_labels.columns)[1:]
# print(class_labels)
print('- which class label you want to do PDV analysis on?')
print('Select from:', ', '.join(class_labels))
class_name = input('class label: ')
if class_name not in list(data_labels.columns):
    print('The class name is not available in the list of labels! Please re-enter or check the spelling.')
    sys.exit()

print()
class_values = list(set(data_labels[class_name].to_list()))
rm = True
while rm:
    print('- do you wish to leave out any label from this analysis - enter the name or type "no" to stop?')
    print('Select from:', ', '.join(class_values))
    class_name_rm = input('label to remove: ')
    if class_name_rm.lower() == 'no': break
    if class_name_rm in class_values:
        class_values.remove(class_name_rm)

# read pdv files from their folder
pdvs = []
if arguments.p[-1] != '/':
    arguments.p += '/' # prepare to read files from the folder
if not os.path.exists(arguments.p):
    print('path {} does not exist! Please check.'.format(arguments.p))
    sys.exit()
for nrn in data_labels['neuron'].to_list():
    with open(arguments.p + "{0:0=3d}neuron_SpatialGraph.CNG.pvec".format(nrn)) as f:
        lines = f.readlines()
    f.close()
    pdvs.append(np.fromstring(lines[1], dtype=float, sep=' '))

## make array of all PDVs
pdvs = np.array(pdvs)

# pdvs=np.loadtxt('./Data/pdvG.txt') # test. read all from a single file if available

# calculated pairwise sumproduct and arcosine
matrix_sp = pdvs.dot(pdvs.T)
matrix_acos = matrix_sp.copy()

# divide each sumproduct by its corresponding sumproduct vectors and calcuate arccosine
for i in range(len(matrix_sp)):
    for j in range(len(matrix_sp)):
        if i==j:
            continue # ignore diagonal
        elif i<j:
            matrix_sp[i,j] = np.nan # below marix's diagonal is enough
            matrix_acos[i,j] = np.nan # below marix's diagonal is enough
        else:      
            matrix_sp[i,j]   = matrix_sp[i,j]/np.sqrt(matrix_sp[i,i] * matrix_sp[j,j])
            matrix_acos[i,j] = np.arccos(matrix_sp[i,j])

# =============================================================================
# %% Calculate T-test
# =============================================================================

### Instructions on changing the class 
## to modify the test for different classes please consider:
## class -> labels to change
## LL -> [ALL, PLL]
## hemisphere -> [left, right]
## region -> [tail, trunk]
## tuning -> [c=caudal, r=rostral]
## please change these values in the code below accordingly

# change these parameters according to the description above
class_to_consider = class_name # example: LL
labels_to_consider = class_values # example: ["ALL", "PLL"]

# loop over the values
within, across = [], []
for i in range(len(matrix_sp)):
    for j in range(len(matrix_sp)):
        if i>j:
            l1 = data_labels[class_to_consider].iloc[i]
            l2 = data_labels[class_to_consider].iloc[j]
            if l1 in labels_to_consider and l2 in labels_to_consider:
                if l1 == l2: # within distances
                    within.append(matrix_acos[i,j])
                else: # across distances
                    across.append(matrix_acos[i,j])
        else:
            continue


# performe T-test
tval, pval = ttest_ind(across, within)

# print the significance
bonf_correction = 4
if bonf_correction*pval < 0.05:
    print ('Significanly different! p-value ({:.2e}) < 0.05'.format(bonf_correction*pval))
else:
    print('Not statistically different!')


# bar plot with errors
labels =  ['within', 'across']
plt.figure('Arccosine - {} ({} vs. {})'.format(class_to_consider, labels_to_consider[0], labels_to_consider[1]))
average = [np.mean(within), np.mean(across)]
err = [np.std(within), np.std(across)]  # for errors
plt.bar(labels, average, yerr=err, 
    color=['blue', 'green'],
    align='center',
    alpha=0.65,
    ecolor='k',
    capsize=5)
# plt.title('Arccosine - {}({} vs. {})'.format(class_to_consider, labels_to_consider[0], labels_to_consider[1]))
# plt.savefig("./figs/pdv-comparision",dpi=500, bbox_inches='tight') # save to file
plt.show()


