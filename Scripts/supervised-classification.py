#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 2021

@author: Kayvan Bijari

Codes and scripts for analysis of journal paper: A neuronal blueprint for directional mechanosensation in larval zebrafish
https://doi.org/10.1016/j.cub.2021.01.045

Supervised classification

This codes reads and normalizes the data.
Reads the labels for the data
Sorts the fatures based on their importance. Feature importance refers to a class of techniques for assigning scores to input features
to a predictive model that indicates the relative importance of each feature when making a prediction. Feature selection is important since
irrelevant or partially relevant features can negatively impact model performance.


lines starting with a sharp (#) are comments

"""

# =============================================================================
# %% required imports
# =============================================================================
import argparse, sys
import pandas as pd # data management

import matplotlib.pyplot as plt # plot and visualization
plt.style.use('ggplot')
fig, ax = plt.subplots(1,2, figsize=(24, 12), num='Supervised Classification') # subplots and figure sizes
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18
plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



# analysis
from sklearn.cluster import KMeans
from sklearn.mixture  import GaussianMixture
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from collections import Counter
import numpy as np

# classifiers
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression

## version of the installed packages:
# cycler==0.10.0
# joblib==1.0.1
# kiwisolver==1.3.1
# matplotlib==3.3.4
# numpy==1.20.1
# pandas==1.2.2
# Pillow==8.1.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# pytz==2021.1
# scikit-learn==0.24.1
# scipy==1.6.0
# six==1.15.0
# sklearn==0.0
# threadpoolctl==2.1.0

# =============================================================================
# %% get inputs from the prompt
# =============================================================================

parser = argparse.ArgumentParser()

parser.add_argument('-i', metavar='input-file', type=argparse.FileType('rt'))
parser.add_argument('-l', metavar='input-labels', type=argparse.FileType('rt'))

try:
    arguments = parser.parse_args()
    if arguments.i == None:
    	print('Input file can not be empty! Please make sure to specify input using: -i <your-input-file>')
    	sys.exit()
    if arguments.l == None:
        print('Label file is not specified! Pleae specify your label data using: -l <your-labels-data>')
        sys.exit()
except Exception as e:
    parser.error(str(e))
    sys.exit()

# =============================================================================
# %% load, normalize, and find principal components of the data
# =============================================================================
data = pd.read_csv(arguments.i) # read the data from the directory it is located

# remove unwanted columns & keep only the numeric values for analysis then normalize the data
x = data.iloc[:, 1:].values # take out the values
x_norm = StandardScaler().fit_transform(x) # normalize the values

# PCA projection to 2D, for visualization purposes
pca = PCA(n_components=2)
x_principal = pca.fit_transform(x_norm)

# read labes for the data
data_labels = pd.read_csv(arguments.l) # read data labels from where it is located

print()
class_labels = list(data_labels.columns)[1:]
# print(class_labels)
print('- which class label you want to perform classification on?')
print('Select from:', ', '.join(class_labels))
class_name = input('class label: ')
if class_name not in list(data_labels.columns):
    print('The class name is not available in the list of labels! Please re-enter or check the spelling.')
    sys.exit()

# =============================================================================
# %% Calculate feature importance & accuracy
# =============================================================================

# plt.figure('Feature importance')
class_label = class_name # select from ['hemisphere', 'LL', 'region', 'neuromast', 'tuning']
y = data_labels[class_label].values
model = ExtraTreesClassifier(n_estimators=25) # initialize the model win 25 estimators for the tree
model.fit(x_norm, y)

# plot graph of feature importances for better visualization
feat_importances = pd.Series(model.feature_importances_, index=data.columns[1:])
print(feat_importances.values)
# feat_importances.nlargest(20).plot(kind='barh')
ax[0].barh(feat_importances.keys(), feat_importances.values)
# ax[0].set_xticklabels(fontsize=BIGGER_SIZE)
ax[0].set_xlabel('Score', fontsize=BIGGER_SIZE)
ax[0].set_ylabel('Features', fontsize=BIGGER_SIZE)
ax[0].set_title('A', loc='left')
plt.tight_layout()


# Classification on all features
all_results = []
print() # empty line
print('Classification on all features:')
# Logistic regression model
log = LogisticRegression(random_state=1, max_iter=1000)
log.fit(x_norm, y)
print('- Accuracy of logistic regression classifier on training set: {:.2f}'.format(log.score(x_norm, y)))
all_results.append(log.score(x_norm, y))
# Decision tree algorithm
DT = DecisionTreeClassifier(random_state=1)
DT.fit(x_norm,y)
print('- Accuracy of Decision Tree classifier on training set: {:.2f}'.format(DT.score(x_norm, y)))
all_results.append(DT.score(x_norm, y))
# K nearest neighbors algorithm
knn = KNeighborsClassifier(n_neighbors = 4)
knn.fit(x_norm,y)
print('- Accuracy of K-NN classifier on training set: {:.2f}'.format(knn.score(x_norm, y)))
all_results.append(knn.score(x_norm, y))
# Multi layer perceptron neural network
mlp = MLPClassifier(random_state=12, max_iter=1000)
mlp.fit(x_norm,y)
print('- Accuracy of MLP classifier on training set: {:.2f}'.format(mlp.score(x_norm, y)))
all_results.append(mlp.score(x_norm, y))


# Classification on the top feature only
top_index = np.argmax(feat_importances)
top_results = []

print() # empty line
print('Classification on top feature ({}): '.format(feat_importances.keys()[top_index]))
# Logistic regression model
log = LogisticRegression(random_state=1, max_iter=1000)
log.fit(x_norm[:, top_index:top_index+1], y)
print('- Accuracy of logistic regression classifier on training set: {:.2f}'.format(log.score(x_norm[:, top_index:top_index+1], y)))
top_results.append(log.score(x_norm[:, top_index:top_index+1], y))
# Decision tree algorithm
DT = DecisionTreeClassifier(random_state=1)
DT.fit(x_norm[:, top_index:top_index+1],y)
print('- Accuracy of Decision Tree classifier on training set: {:.2f}'.format(DT.score(x_norm[:, top_index:top_index+1], y)))
top_results.append(DT.score(x_norm[:, top_index:top_index+1], y))
# K nearest neighbors algorithm
knn = KNeighborsClassifier(n_neighbors = 4)
knn.fit(x_norm[:, top_index:top_index+1],y)
print('- Accuracy of K-NN classifier on training set: {:.2f}'.format(knn.score(x_norm[:, top_index:top_index+1], y)))
top_results.append(knn.score(x_norm[:, top_index:top_index+1], y))
# Multi layer perceptron neural network and its parameters
mlp = MLPClassifier(random_state=12, max_iter=1000)
mlp.fit(x_norm[:, top_index:top_index+1],y)
print('- Accuracy of MLP classifier on training set: {:.2f}'.format(mlp.score(x_norm[:, top_index:top_index+1], y)))
top_results.append(mlp.score(x_norm[:, top_index:top_index+1], y))


classifiers = ['Logistic Regression', 'Decision Tree', 'KNN', 'MLP']
inds = np.arange(len(classifiers))
width = 0.35
ax[1].bar(inds, all_results, width = width, label='All features')
ax[1].bar(inds+width, top_results, width = width, label='Top feature')
# ax[1].set_yticks([0, .2, .4, .6, .8,  1, 1.1])
# ax[1].set_yticklabels([0, .2, .4, .6, .8, 1, ''])
# ax[1].set_yticks(np.arange(0,1.1,.1))
ax[1].set_ylabel('Accuracy', fontsize=BIGGER_SIZE)
ax[1].set_xlabel('Algorithms', fontsize=BIGGER_SIZE)
ax[1].set_xticks(inds + width / 2 )
ax[1].set_xticklabels(classifiers, fontsize=BIGGER_SIZE)
ax[1].legend()
ax[1].set_title('B', loc='left')

for tick in ax[0].get_yticklabels():
    tick.set_fontsize(BIGGER_SIZE)

for tick in ax[1].get_yticklabels():
    tick.set_fontsize(BIGGER_SIZE)

for tick in ax[0].get_xticklabels():
    tick.set_fontsize(BIGGER_SIZE)

# show the figure
plt.tight_layout()
# plt.savefig("./figs/classification",dpi=500, bbox_inches='tight') # save to file
plt.show()
